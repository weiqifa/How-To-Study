
# 1. 介绍

这是一个学习资源的汇总项目，设计的内容不仅仅是Linux

# 2. Linux 内核学习

* http://www.oldlinux.org/
* https://lkml.org/
* https://github.com/ljrcore/linuxmooc
* 一个比较优秀的Linux内核解析文章，https://github.com/0xAX/linux-insides

内核代码阅读网站

* https://elixir.bootlin.com/linux/latest/source

韦老师的免费视频可以先看看，当然了，如果有条件的，可以去看看收费视频，除此之外，学习的时候，至少要自己下载一套Linux内核代码，以目前的情况，至少要搞一套4.4内核之后的代码。

陈老师的学习视频

* https://www.bilibili.com/video/BV1Pa4y1e72U?p=10


另一个Linux基础视频

* https://www.bilibili.com/video/BV1CK4y1j7TZ/?spm_id_from=autoNext

Linux 设备驱动

* https://blog.csdn.net/kris_fei/category_7110058.html

* https://blog.csdn.net/lee_jimmy/category_9279745.html

搞开源项目的论坛

这是一个开源的Linux论坛，里面的内容非常优秀，喜欢做开源的，或者喜欢Linux的，都可以看看。

* https://whycan.com/index.html


# 3. Android && Linux 相关

学习Android系统源码，需要掌握系统源码目录。
可以访问 http://androidxref.com 来阅读系统源码，当然，最好是将源码下载下来。

这里我推荐使用百度网盘地址 http://pan.baidu.com/s/1ngsZs 进行下载，目前其中提供了Android 1.6到Android 8.1.0多个Android版本的源码。

* https://blog.csdn.net/vviccc/article/details/103477944

## 3.1    Audio相关

* https://blog.csdn.net/kris_fei/category_6987006_2.html
* https://blog.csdn.net/DroidPhone/category_1118446.html
* https://blog.csdn.net/vviccc/article/details/105468626
* https://blog.csdn.net/vviccc/article/details/105492383?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-0&spm=1001.2101.3001.4242

## 3.2    Android Camera相关

* https://blog.csdn.net/kris_fei/category_6987007.html

## 3.3    Android binder
* https://blog.csdn.net/liyuntonglyt/article/details/61912822?locationNum=9&fps=1


# 4. 单片机相关，如果是小白入门最好从这里开始

我自己整理的两个单片机项目

* https://github.com/weiqifa0/FQ-BalanceCar

* https://github.com/Stc89/Basics

* 使用51单片机来做USB鼠标，https://github.com/lisongze2016/mcu_project

## 4.1 Keil 相关
* https://blog.csdn.net/ybhuangfugui/category_9267298.html



# 5. 无线

## 5.1.    Zigbee芯科Zigbee 相关

* https://github.com/MarkDing/IoT-Developer-Boot-Camp

* https://github.com/MarkDing/IoT-Developer-Boot-Camp/wiki/Zigbee-Hands-on-Using-Event-CN

* https://blog.csdn.net/lee_jimmy/article/details/109474784

* https://blog.csdn.net/sinat_23338865/article/details/109998541

## 5.2.    蓝牙相关
* https://blog.csdn.net/xubin341719/article/details/38305331


# 6.毕业设计

这个是关于毕业设计的一个网站，里面的内容挺有意思，不过写的代码比较一般，我下过几个设计。对于新手来说是可以作为学习的资料的。

* https://www.bsdog.cn/

# 7.RTOS 

## 7.1 Freertos 相关
* https://blog.csdn.net/ybhuangfugui/category_9269886.html

# 8.国外比较好的嵌入式学习网站

* https://www.reddit.com/r/embedded/

* https://www.reddit.com/r/kernel/

* https://lkml.org/

# 9.面试常见的一些问题
* mmap 原理，https://www.zhihu.com/question/48161206
* malloc 原理，https://blog.csdn.net/weixin_43869778/article/details/107735367?utm_source=app&app_version=4.5.5
* Linux 内存分配
* Linux 的下的调度算法，https://mp.weixin.qq.com/s/5xG8kCaFeLuvwV6W02wPcA
* Linux 文件系统
* Linux 内核线程
* Alsa 框架
* 音频的3A算法